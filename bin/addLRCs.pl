use strict;
use warnings;

open(my $in, "<", "$ARGV[0]") or die "Can't open list.";

my @list = <$in>;
close($in) or die "Can't close $in.";

open(OUTPUT, ">", "$ARGV[1]") or die "Can't access output file.";

open(my $in2, "<", "$ARGV[2]") or die "Can't open lrcs.";

my @lrcs = <$in2>;
close($in2) or die "Can't close $in2.";

for ( my $j = 0; $j <= $#list; $j++ ) {
	chomp $list[$j];
	print OUTPUT $list[$j] . "\n";
	if ( $list[$j] eq "N.CONSTRAINTS" ) {
		for ( my $k = 0; $k <= $#lrcs; $k++) {
			chomp $lrcs[$k];
			print OUTPUT $lrcs[$k] . "\n";
		}
	}
}

close(OUTPUT) or die "Can't close OUTPUT.";
