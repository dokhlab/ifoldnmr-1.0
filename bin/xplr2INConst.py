'''
xplr2INConst.py

by Benfeard Williams
August 17, 2015

This script will convert RNA xplor NMR constraints
into DMD constraints for all-atom simulations

Example seq input line
G
U
A
G
END

Example bp input line:
A3 A18
A4 A17

Example xplr input line:
assi (resid   3 and name   N1) (resid  18 and name   H3)  2.00 0.20 0.20 ! A-U basepair
assign (residue 4 and name H1)(residue 18 and name H3)     3.3  0.7  0.7

Use 3bead base pair constraints

usage: python xplr2INConst.py 1YG3.seq 1YG3.bp 1YG3.mr

version 1.0 -- it works
version 1.1 -- check # of nucleotides in RNA to avoid contraints involving small molecules
version 1.2 -- check if atom type is part of that DMD nucleotide b/c DMD does not use modified
				 species such as protonated cytosine 
version 1.3 -- basic handling of improper usage
version 1.4 -- ensures proper numbering of two chain RNA
version 1.5 -- updated handling of two chain RNA
'''

import sys
import re

#Check the number of arguments given
if len(sys.argv) != 4:
	print "Improper number of arguments given"
	print "Usage: python xplr2INConst.py 1YG3.seq 1YG3.bp 1YG3.mr"
	sys.exit()

#Nucleic Acid atom types from DMD
#DMD atom H2' has been purposefully renamed to HO2'
ADE = set(["P","O1P","O2P","C1'","C2'","O2'","HO2'","C3'","O3'","C4'","O4'","C5'","O5'","N1","C2","N3","C4","C5","C6","N6","H61","H62","N7","C8","N9"]+["H1'","H2","H8"])
URI = set(["P","O1P","O2P","C1'","C2'","O2'","HO2'","C3'","O3'","C4'","O4'","C5'","O5'","N1","C2","O2","N3","H3","C4","O4","C5","C6"]+["H1'","H8"])
CYS = set(["P","O1P","O2P","C1'","C2'","O2'","HO2'","C3'","O3'","C4'","O4'","C5'","O5'","N1","C2","O2","N3","C4","N4","H41","H42","C5","C6"]+["H1'","H6"])
GUA = set(["P","O1P","O2P","C1'","C2'","O2'","HO2'","C3'","O3'","C4'","O4'","C5'","O5'","N1","H1","C2","N2","H21","H22","N3","C4","C5","C6","O6","N7","C8","N9"]+["H1'","H6"])

#Is this atom included in this nucleotide?
def checkAtom(atomName, resName):
	if resName == 'G':
		return atomName in GUA
	if resName == 'A':
		return atomName in ADE
	if resName == 'C':
		return atomName in CYS
	if resName == 'U':
		return atomName in URI

#Read the sequence file
sequence = open(sys.argv[-3])
nuc = {}
counter = 1
chainA = 0 #length of chain A
#Store the resNum and resType in a dictionary
for line in sequence:
	if line[0:3] == "TER":
		chainA = len(nuc)
		continue
	if line[0:3] != "END":
		nuc[str(counter)] = line.strip()
		counter += 1
	else:
		break
if chainA == 0: chainA = len(nuc)
sequence.close()

#Read the base pairs file
basePairs = open(sys.argv[-2])
BP_list = []
#Add all base pairs to a list
for line in basePairs:
	line = line.split()
	if chainA != 0 and line[1][0] == 'B':
		line[1] = 'B' + str(int(line[1][1:])+chainA)
	if chainA != 0 and line[0][0] == 'B':
		line[0] = 'B' + str(int(line[0][1:])+chainA)
	#line = line.strip().split('A')
	#BP_list.append([line[1].strip(),line[2]])
	BP_list.append([line[0][1:],line[1][1:]])

#Read the NMR constraints files
xplr = open(sys.argv[-1])
for line in xplr:
	if line[0:4] == "assi":
		line = line.replace('(',' ').replace(')',' ').split()
		#Check if constraints involve base pairs
		if [line[2],line[7]] in BP_list or [line[7],line[2]] in BP_list:
			#print "is a base pair"
			continue
		#Check if constraint is within sequence length
		#to avoid constraints with other molecules
		if line[2] not in nuc or line[7] not in nuc:
			#print "nucleotide not in sequence"
			continue
		if not checkAtom(line[5],nuc[line[2]]) or not checkAtom(line[10],nuc[line[7]]):
			#print "atom not in nucleotide"
			continue
		#Correct atom name for HO2*
		if line[5] == "HO2\'":
			line[5] = "H2\'"
		if line[10] == "HO2\'":
			line[10] = "H2\'"
		#Correct number if nucleotide is in second chain
		if int(line[2]) > chainA:
			line[2] = "2." + str(int(line[2]) - chainA)
		if int(line[7]) > chainA:
			line[7] = "2." + str(int(line[7]) - chainA)
		if line[2][:2] != "2." and int(line[2]) <= chainA: line[2] = "1." + line[2]
		if line[7][:2] != "2." and int(line[7]) <= chainA: line[7] = "1." + line[7]
		#Distance measures and deviations
		dist = float(line[11])
		minus = float(line[12])
		plus = float(line[13])
		#Depth of each potential well step in kcal/mol
		step = -0.6
		#Print DMD AtomPair constraints
		if plus == 0.0:
			print "AtomPair %s.%s %s.%s %5.6f %5.6f" % (line[2], line[5].replace('\'','*'), line[7], line[10].replace("\'",'*'), dist-minus, dist)
		if plus != 0.0:
			print "AtomPair %s.%s %s.%s %5.6f %5.6f %5.6f %5.6f %5.6f %5.6f %5.6f %5.6f %5.6f %5.6f %5.6f %5.6f" % (
			line[2], line[5].replace('\'','*'), line[7], line[10].replace("\'",'*'), dist-minus, dist, step, dist+plus, step, dist+plus*2, step, dist+plus*3, step, dist+plus*4, step, dist+plus*5)
xplr.close()