#Benfeard Williams
#Tue Jul 12 2016
#
#this script converts base pair file from A format to BasePair format
#
import sys

file = open( sys.argv[-1] )

for line in file:
	# example line: "A62	A4"
	# example results: "BasePair 1.62	1.4"
	line = line.strip().replace("A", "1.")
	# now we have: "1.62	1.4"
	line = "BasePair " + line
	print line