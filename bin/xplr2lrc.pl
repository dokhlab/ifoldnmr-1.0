#!/usr/bin/perl

##########################################################
## The program reads the xplor input and output the LRC ##
## (long-range constraints) for 3-bead RNA modeling.    ##
##                                                      ##
## Feng Ding<fding@clemson.edu>                         ## 
##   -- Copyright 2012;                                 ## 
##########################################################           

if(@ARGV<3){
    printf("usage: xplor2lrc.pl seq bp_list xplor_list\n");
    printf("   seq        -- 1-column sequence file\n");
    printf("   bp_list    -- 2-column base pair file\n");
    printf("   xplor_list -- xplr input file\n");
    exit(1);
}

my @seq;
my @chains;
my $nch=0;
my $index=0;
$chains[$nch++]=$index;
open(in,"<$ARGV[0]");
while(<in>){
    chomp();
    if($_ eq "A" || $_ eq"ADE" ||
       $_ eq "U" || $_ eq"URI" ||
       $_ eq "G" || $_ eq"GUA" ||
       $_ eq "C" || $_ eq"CYT" ){
	$seq[$index++] = substr($_,0,1);
    }
    else{
	if($_ eq "TER" || $_ eq "END"){
	    $chains[$nch++] = $index;
	}
    }
}
if($chains[$nch-1] != $index){
    $chains[$nch++] = $index;
}
close(in);
#print $index . " " . $nch . "\n";

my @bp;
open(in, "<$ARGV[1]");
while(<in>){
    chomp();
    my @tmp_array = split(" ", $_);
    #printf "$tmp_array[0] $tmp_array[1] \n";
    my $i=-1;
    my $j=-1;
    if($tmp_array[0] =~ /^[A-Za-z]/ ){
	my $ch = uc(substr($tmp_array[0],0,1));
	$ch = ord($ch)-ord("A");
	#print "$ch $chains[$ch]\n";
	$i = $chains[$ch]+substr($tmp_array[0],1)-1;
    }
    else{
	$i = int($tmp_array[0])-1;
    }   
    if($tmp_array[1] =~ /^[A-Za-z]/ ){
	my $ch = substr($tmp_array[1],0,1);
	$ch = ord($ch)-ord("A");
	#print "$ch $chains[$ch]\n";
	$j = $chains[$ch]+substr($tmp_array[1],1)-1;
    }
    else{
	$j = int($tmp_array[1]-1);
    }   
    if ($i>@seq || $j>@seq){
	printf("wrong basepair: $_\n");
	exit(1);
    }
    #print "$i $seq[$i]  $j $seq[$i]\n";
    if(($seq[$i] eq "A" && $seq[$j] eq "U") ||
       ($seq[$i] eq "U" && $seq[$j] eq "A") ||
       ($seq[$i] eq "G" && $seq[$j] eq "C") ||
       ($seq[$i] eq "C" && $seq[$j] eq "G") ||
       ($seq[$i] eq "G" && $seq[$j] eq "U") ||
       ($seq[$i] eq "U" && $seq[$j] eq "G") ){
	#print "$i  $j\n";	
	$bp[$i][$j] = 1;
	$bp[$j][$i] = 1;
    }
    else{
	printf("wrong baspair:$i+1 $seq[$i]  $j+1 $seq[$i]\n");
	exit(1);
    }
}
close(in);

open(in, "<$ARGV[2]");
my @constraints;
while(<in>){
    chomp();
    if($_ =~ /^assi/){
	$_ =~ s/[()]/ /g;
	my @tmp = split(" ",$_);
	#print "$_ \n";
	my $ri = $tmp[2];
	my $ai = $tmp[5];
	
	my $rj = $tmp[7];
	my $aj = $tmp[10];
	#printf("$ri $ai $rj $aj\n");
	
	if(abs($ri-$rj)>1){#non-local
	    if(!defined($bp[$ri-1][$rj-1]) && 
	       !defined($bp[$ri-1][$rj]) && !defined($bp[$ri][$rj-1]) &&
	       ($rj>1 && !defined($bp[$ri-1][$rj-2])) && ($ri>1 && !defined($bp[$ri-2][$rj-1]))	){#non-hbond
		#printf("$ri $ai $rj $aj $_\n");
		my $sufix_ai = substr($ai,length($ai)-1,1);
		my $i_sb = 'B';
		my $ii = 2*($ri-1)+1;
		#if($sufix_ai eq '\'' || $sufix_ai eq '*'){#sugar
		if($sufix_ai eq '\''){#sugar
		    #printf("$sufix_ai\n");
		    $ii = 2*($ri-1);
		    $i_sb = 'S';
		}
		
		my $sufix_aj = substr($aj,length($aj)-1,1);
		my $j_sb = 'B';
		my $jj = 2*($rj-1)+1;
		#if($sufix_aj eq '\'' || $sufix_aj eq '*'){#sugar
		if($sufix_aj eq '\''){#sugar
		    #printf("$sufix_aj\n");
		    $jj = 2*($rj-1);
		    $j_sb = 'S';
		}
		
		my $d=$tmp[11];
		my $d_minus=$tmp[12];
		my $d_plus=$tmp[13];
		
		if(!defined($constraints[$ii][$jj])){
		    $constraints[$ii][$jj] = $d+$d_plus;
		    $constraints[$jj][$ii] = $d+$d_plus;
		}
		else{
		    my $dd = $d+$d_plus;
		    if($dd > $constraints[$ii][$jj]){
			#print("$ri $rj $constraints[$ii][$jj] $dd \n");
			$constraints[$ii][$jj] = $dd;
		    }
		}
		#printf("$char_ci$ri $i_sb $char_cj$rj $j_sb %f\n", $d+$d_plus);
	    }
	    else{
		#printf("$ri $ai $rj $aj $_\n");
	    }
	}
    }
}
close(in);


for(my $i=0; $i<2*$index; $i++){
    for(my $j=0; $j<$i; $j++){
	if(defined($constraints[$i][$j])){
	    my $ri = int($i/2);
	    my $bi = $i % 2;
	    my $ci;
	    for(my $i=$nch-1; $i>=0; $i--){
		#print("$ri $chains[$i]\n");
		if($ri > $chains[$i]){
		    $ci = $i;
		    $ri -= $chains[$i];
		    last;
		}
	    }
	    my $char_ci = chr(ord('A')+$ci);
	    my $i_sb = 'B';
	    if($bi==0){
		$i_sb = 'S';
	    }
	    
	    my $rj = int($j/2);
	    my $bj = $j % 2;
	    my $cj;
	    for(my $i=$nch-1; $i>=0; $i--){
		#print("$ri $chains[$i]\n");
		if($rj > $chains[$i]){
		    $cj = $i;
		    $rj -= $chains[$i];
		    last;
		}
	    }
	    my $char_cj = chr(ord('A')+$cj);
	    my $j_sb = 'B';
	    if($bj==0){
		$j_sb = 'S';
	    }

	    my $modify=5.5;
	    if($i_sb eq 'S' && $j_sb eq 'S'){
		$modify = 5.0
	    }
	    if($i_sb eq 'B' && $j_sb eq 'B'){
		$modify = 6.0
	    }
	    
	    my $deltaE = 6.0;
	    my $deltaL = 30.0;
	    my $nstep  = 24.0;
	    
	    printf("$char_ci%d $i_sb \t$char_cj%d $j_sb \tATT IR %f \tDE %f \tDL %f \tNSTEP %d\n", 
		   $ri+1, $rj+1, $constraints[$i][$j]+$modify, $deltaE, $deltaL, $nstep);
	}
    }
}

