#!/usr/bin/env python

import sys

from operator import itemgetter
from scipy.optimize import fmin
from scipy.optimize import leastsq
import numpy as np

#this value is used as the initial guess for least squares fitting; may need to be adjusted depending on the RNA
mean_test_value = 20

def findCutoff(seq_file):
    seq = open(seq_file, "r")
    seq = seq.readlines()
    length = len(seq) - 1
    return (5.1*((length)**(0.41))-15.8+1.8) #taken from Hajdin et al., RNA, 2010

def read_echo_list(name):
    echo_list = []
    for line in open(name, "r"):
        echo_list.append(line.strip().split())
    return echo_list

def gen_echo_hist(data):
    
    echo_hist = []
    #generate echo histogram
    for i in range(0, 100):
        echo_hist.append([i, 0])
        
    for i in range(len(data)):
        rd_gyr = float(data[i][3])
        
        for i in range(1, len(echo_hist)):
            if echo_hist[i-1][0] <= rd_gyr and echo_hist[i][0] > rd_gyr:
                echo_hist[i][1] += 1
                break
    
    return echo_hist

def norm(x, A, mean, sd):
    norm = []
    for i in range(len(x)):
        norm += A*[1.0/(sd*np.sqrt(2*np.pi))*np.exp(-(x[i] - mean)**2/(2*sd**2))]
    return np.array(norm)

def findMaxValue(data): #finds maximum index value based on list[i][1]
    
    max_value = 0
    
    for i in range(len(data)):
        if data[i][1] > max_value:
            max_value = data[i][1]
            
    return max_value

def scale_to_one(data):
    max_value = findMaxValue(data)
    for i in range(len(data)):
        data[i][1] = (float(data[i][1]))/max_value
    return data

def findScaleDiff(rx, bg):
    
    final_scaled = []            
    rx_list = []
    bg_list = []
    
    for i in range(len(rx)):
        rx_list.append(rx[i][1])
        bg_list.append(bg[i][1])
    
    factor = 1.0
    
    resultList = fmin(scaleFactorFunction, factor, args=(rx_list,bg_list),full_output=1,disp=0)
    scaleFactor = resultList[0][0]

    for i in range(len(rx)):
        scaled_value = rx[i][1] - scaleFactor*bg[i][1]
        if scaled_value > 0:
            final_scaled.append([rx[i][0], scaled_value])
        else:
            final_scaled.append([rx[i][0], 0])
        
    return final_scaled

def scaleFactorFunction (factor, A, B):
    err = np.sum( np.abs (A - factor * B))
    return err 

def res(p, y, x):
    A1, m1, sd1 = p
    y_fit = norm(x, A1, m1, sd1)
    err = y - y_fit
    return err

seq_file = sys.argv[1]
clust_cutoff = findCutoff(seq_file)

exp_echo = []
for i in range(2, len(sys.argv)-1):
    temp = open(sys.argv[i], "r")
    mov_file = sys.argv[i].split(".echo")
    mov_file = mov_file[0] + ".mov"
    temp = temp.readlines()
    temp.pop(0)
    for line in temp:
        line = line.strip().split()
        line.append(mov_file)
        exp_echo.append(line)

#bg_echo = read_echo_list(sys.argv[len(sys.argv)-2])

#exp_hist = gen_echo_hist(exp_echo)
#bg_hist = gen_echo_hist(bg_echo)

#diff_hist = findScaleDiff(exp_hist, bg_hist)
#diff_hist = scale_to_one(diff_hist)

#A1, m1, sd1 = [1, mean_test_value, 10]
#p = [A1, m1, sd1]

#x = []
#y = []
#for line in diff_hist:
#    x += [line[0]]
#    y += [line[1]]
#x = np.array(x)
#y = np.array(y)

#plsq = leastsq(res, p, args = (y, x))
#p = plsq[0]

#average = p[1]
#std_dev = p[2]

pdb_list = []
for line in exp_echo:
        #if float(line[3]) >= (average - std_dev) and float(line[3]) <= (average + std_dev):
            frame = float(line[0])
            energy = float(line[2])
            mov_file = line[10]
            pdb_list.append([mov_file, frame, energy])
pdb_list = sorted(pdb_list, key=itemgetter(2))

output_name = sys.argv[len(sys.argv)-1]

#hist_out = open(output_name + "_hist.txt", "w")

#hist_out.write("Determined average: " + str(average) + "\n")
#hist_out.write("Determined standard deviation: " + str(std_dev) + "\n")
#hist_out.write("BIN\tWithCONST\tWithoutCONST\n")

#for i in range(len(exp_hist)):
#    hist_out.write(str(exp_hist[i][0]) + "\t" + str(exp_hist[i][1]) + "\t" + str(bg_hist[i][1]) + "\n")
    
#hist_out.close()

clustering_script = output_name + ".sh"

pdb_gen = open(clustering_script, "w")
pdb_gen.write("#!/bin/bash\n\n")
for i in range(250):
    frame = int((pdb_list[i][1] - pdb_list[i][1]%10)/10) + 1
    pdb_name = output_name + "_" + str(i) + ".pdb"
    mov_name = pdb_list[i][0]
    pdb_gen.write("../bin/nseq_movie2pdb.linux " + mov_name + " " + seq_file + " " + pdb_name + " " + str(frame) + " 1 1\n")
pdb_gen.close()

clustering = open(clustering_script, "a")

oc_input_name = output_name + "_oc_input.txt"
oc_output_name = output_name + "_oc_output.txt"
clust_analysis_name = output_name + "_clust_analysis.txt"

clustering.write("echo \"" + str(250) + "\" > " + oc_input_name + "\n")

for i in range(250):
    pdb_name = output_name + "_" + str(i) + ".pdb"
    clustering.write("echo \"" + pdb_name + "\" >> " + oc_input_name + "\n")
    
for i in range(250):
    for j in range(i+1, 250):
        pdb_name_1 = output_name + "_" + str(i) + ".pdb"
        pdb_name_2 = output_name + "_" + str(j) + ".pdb"
        
        clustering.write("../bin/getRMSD.linux " + pdb_name_1 + " " + pdb_name_2 + " >> " + oc_input_name + "\n")
        
clustering.write("../bin/oc dis complete < " + oc_input_name + " > " + oc_output_name + "\n")
clustering.write("../bin/analyze-oc.pl " + oc_input_name + " " + oc_output_name + " DIST CUTOFF " + str(clust_cutoff) + " > " + clust_analysis_name + "\n")
