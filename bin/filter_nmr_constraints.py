'''
usage: python filter_nmr_constraints.py xplorFile > filteredFile
'''
import sys

skip_list = ["H6","H8","H2","H21","H22","H41","H42","H2","H61","H62"]
counter = 0
xplr = open(sys.argv[-1])
for line in xplr:
	if line[0:4] == "assi":
		line = line.replace('(',' ').replace(')',' ').split()
		if line[5] in skip_list or line[10] in skip_list:
			continue
		counter += 1
		print line[0] + " ( " + " ".join(line[1:6]) + " ) ( " + " ".join(line[6:11]) + " ) " + " ".join(line[11:])
	else:
		print line.rstrip('\n')
#print "number of constraints is ", str(counter)
xplr.close()
