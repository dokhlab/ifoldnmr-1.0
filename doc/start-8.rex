#NUMBER OF REPLICA
N_REPLICA       8

#TIME FOR PERFORMING REPLICA EXCHANGE
RX_DT           1000

#REPLICA INPUT-STATE file
#Without specifications, the state file for each repical will be 
# set the input txt file in the command-line by DEFAULT, 
#REPLICA_TXT 0	p000.iFoldNMR.txt
#REPLICA_TXT 1	p001.iFoldNMR.txt
#REPLICA_TXT 2	p002.iFoldNMR.txt
#REPLICA_TXT 3	p003.iFoldNMR.txt
#REPLICA_TXT 4	p004.iFoldNMR.txt
#REPLICA_TXT 5	p005.iFoldNMR.txt
#REPLICA_TXT 6	p006.iFoldNMR.txt
#REPLICA_TXT 7	p007.iFoldNMR.txt

#REPLICA TEMPERATURE SETUP
#KEYWORD(REPLICA) INDEX(0-N_REPLICA) TEMPERATURE
# the temperature specified in task file will be overwritten
REPLICA    0       0.200
REPLICA	   1	   0.225
REPLICA    2       0.250
REPLICA    3       0.270
REPLICA    4       0.300
REPLICA    5       0.333
REPLICA    6       0.367
REPLICA    7       0.400

#RX_TEMP_OUT
RX_OUT          RX_TEMP.out
